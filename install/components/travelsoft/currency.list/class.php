<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use travelsoft\currency\stores\Courses;

Loc::loadMessages(__FILE__);

class TravelsoftCurrencyList extends CBitrixComponent
{
    /**
     * @return mixed|void
     * @throws \Bitrix\Main\LoaderException
     * @throws Exception
     */
    public function executeComponent()
    {
        if (!Loader::includeModule('travelsoft.currency')) {
            throw new Exception(Loc::getMessage('TSCS_MOIDULE_NOT_FOUND'));
        }

        $courses = array_values(Courses::get(['filter' => ['UF_ACTIVE' => true], 'order' => ['ID' => 'DESC'], 'limit' => 2]));

        $this->arResult['DISPLAY_CURRENCIES'] = array_map(function (string $displayCurrency): array {
            return [
                'lower_field' => strtolower($displayCurrency),
                'field' => $displayCurrency,
                'uf_field' => "UF_{$displayCurrency}"
            ];
        }, $this->arParams['DISPLAY_CURRENCIES']);

        if (isset($courses[0])) {
            $flippedDisplayCurrencies = array_flip(array_column($this->arResult['DISPLAY_CURRENCIES'], 'uf_field'));

            $formattedFunction = function ($course) {
                return number_format($course, 5);
            };

            $this->arResult['DATE_LAST_UPDATE'] = date('d.m.Y', $courses[0]['UF_UNIX_DATE']);
            $this->arResult['CURRENT_COURSES'] = array_map($formattedFunction, array_intersect_key($courses[0], $flippedDisplayCurrencies));

            if (isset($courses[1])) {
                $this->arResult['PAST_COURSES'] = array_map($formattedFunction, array_intersect_key($courses[1], $flippedDisplayCurrencies));

                $diffCourses = [];
                foreach ($this->arResult['DISPLAY_CURRENCIES'] as $displayCurrency) {
                    $diffCourses[$displayCurrency['uf_field']] = (isset($this->arResult['CURRENT_COURSES'][$displayCurrency['uf_field']])
                        && isset($this->arResult['PAST_COURSES'][$displayCurrency['uf_field']])) ?
                        $this->arResult['CURRENT_COURSES'][$displayCurrency['uf_field']] - $this->arResult['PAST_COURSES'][$displayCurrency['uf_field']] : '';
                }

                $this->arResult['DIFF_COURSES'] = array_map($formattedFunction, $diffCourses);
            }
        } else {
            $this->arResult['ERRORS'][] = 'NO_COURSES_FOUND';
        }

        $this->IncludeComponentTemplate();
    }

}
