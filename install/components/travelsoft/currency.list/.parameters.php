<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arCurrentValues */

\Bitrix\Main\Loader::includeModule('travelsoft.currency');

$currencies = \travelsoft\currency\stores\Currencies::get([], function (&$currency) {
    $currency = $currency['UF_ISO'];
});

$currencies = array_combine($currencies, $currencies);

$arComponentParameters['PARAMETERS']['DISPLAY_CURRENCIES'] = array(
    "PARENT" => "BASE",
    "NAME" => GetMessage("WHICH_DISPLAY_CURRENCIES"),
    "TYPE" => "LIST",
    "MULTIPLE" => "Y",
    "VALUES" => $currencies
);